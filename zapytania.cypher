ZADANIE 1
MATCH (m:Movie) RETURN m

ZADANIE 2
MATCH (hugo:Person {name: "Hugo Weaving"})-[:ACTED_IN]->(m:Movie) RETURN m

ZADANIE 3
MATCH (hugo:Person {name: "Hugo Weaving"})-[:ACTED_IN]->(m:Movie)<-[:DIRECTED]-(director) RETURN DISTINCT director

ZADANIE 4
MATCH (hugo:Person {name: "Hugo Weaving"})-[:ACTED_IN]->(m:Movie)<-[:ACTED_IN]-(coactor) RETURN DISTINCT coactor

ZADANIE 5
MATCH (n:Movie)<-[:ACTED_IN]-(actors:Person)-[:ACTED_IN]->(m:Movie {title:"The Matrix"}) RETURN DISTINCT n

ZADANIE 6
MATCH (actors:Person)-[r:ACTED_IN]->(Movies) RETURN actors, COUNT(r)

ZADANIE 7
MATCH (p:Person)-[:WROTE|:DIRECTED]->(m:Movie) RETURN DISTINCT p, m.title 

ZADANIE 8
MATCH (h:Person {name: "Hugo Weaving"})-[:ACTED_IN]->(m:Movie)<-[:ACTED_IN]-(r:Person {name: "Keanu Reeves"}) RETURN DISTINCT m


ZADANIE 9
CREATE (FirstAvenger:Movie {title:'Captain America: The First Avenger', released:2011, tagline:'I Could Do This All Day'})
CREATE (Johnston:Person {name:'Joe Johnston', born:1950})
CREATE (Markus:Person {name:'Christopher Markus', born:1970})
CREATE (McFeely:Person {name:'Stephen McFeely', born:1969})
CREATE (Feige:Person {name:'Kevin Feige', born:1973})
CREATE (Evans:Person {name:'Chris Evans', born:1981})
CREATE (Atwell:Person {name:'Hayley Atwell', born:1982})
CREATE (Stan:Person {name:'Sebastian Stan', born:1982})
CREATE (LeeJones:Person {name:'Tommy Lee Jones', born:1946})

CREATE
(LeeJones)-[:ACTED_IN {roles:['Colonel Chester Phillips']}]->(FirstAvenger),
(Stan)-[:ACTED_IN {roles:['James Buchanan Bucky Barnes']}]->(FirstAvenger),
(Atwell)-[:ACTED_IN {roles:['Peggy Carter']}]->(FirstAvenger),
(Evans)-[:ACTED_IN {roles:['Captain America / Steve Rogers']}]->(FirstAvenger),
(Hugo)-[:ACTED_IN {roles:['Johann Schmidt / Red Skull']}]->(FirstAvenger),
(Johnston)-[:DIRECTED]->(FirstAvenger),
(Markus)-[:WROTE]->(FirstAvenger),
(McFeely)-[:WROTE]->(FirstAvenger),
(Feige)-[:PRODUCED]->(FirstAvenger)

MATCH (p:Person)-[r]-(m:Movie {title:"Captain America: The First Avenger"}) RETURN  p,r,m

